import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

import "Components"

ApplicationWindow {
    id: root
    visible: true

    width: units.gu(45)
    height: units.gu(75)

    header: Header {
        title: stackView.currentItem.title
        showBack: stackView.depth > 1
        showAbout: stackView.currentItem.objectName != 'aboutPage'

        onBackButtonClicked: stackView.pop()
        onAboutButtonClicked: stackView.push(Qt.resolvedUrl('AboutPage.qml'))
    }

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: ListPage {}

        //Don't use transitions on changing the page in the stack
        pushEnter: Transition {}
        pushExit: Transition {}
        popExit: Transition {}
        popEnter: Transition {}
    }
}
