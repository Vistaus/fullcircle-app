import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu
import Ubuntu.DownloadManager 1.2
import U1db 1.0 as U1db

import "utils.js" as Utils
import "Components"

Page {
    id: issuePage
    title: issue.title

    property var issue
    property var downloading
    property var downloadUrl
    property var description

    U1db.Database {
        id: downloadsDb
        path: "fullcircle.bhdouglass.downloads"
    }

    Component.onCompleted: {
        description = Utils.parseDescription(issue.description);
    }

    Flickable {
        anchors.fill: parent
        contentHeight: column.height + units.gu(4)
        clip: true

        ColumnLayout {
            id: column
            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                topMargin: units.gu(2)
            }

            Item {
                Layout.fillWidth: true
                Layout.leftMargin: units.gu(6)
                Layout.rightMargin: units.gu(6)
                Layout.preferredHeight: ubuntuShapeIcon.height

                Ubuntu.UbuntuShape {
                    id: ubuntuShapeIcon

                    anchors.centerIn: parent
                    width: (units.gu(30) > parent.width) ? parent.width : units.gu(30)
                    height: width * (7/8)

                    image: Image {
                        id: cover
                        source: Qt.resolvedUrl(issue.image)

                        fillMode: Image.PreserveAspectFit
                        sourceSize {
                            width: ubuntuShapeIcon.width
                            height: ubuntuShapeIcon.height
                        }
                    }
                }

                Image {
                    anchors.fill: ubuntuShapeIcon
                    visible: cover.progress < 1

                    source: Qt.resolvedUrl('../assets/emptyImage.svg')
                    fillMode: Image.PreserveAspectFit
                    sourceSize {
                        width: ubuntuShapeIcon.width
                        height: ubuntuShapeIcon.height
                    }
                }
            }

            Label {
                Layout.fillWidth: true
                Layout.leftMargin: units.gu(6)
                Layout.rightMargin: units.gu(6)
                Layout.topMargin: units.gu(1)

                text: i18n.tr('This Month')
                horizontalAlignment: Label.AlignHCenter
                font.pointSize: units.dp(15)
            }

            Repeater {
                model: description
                delegate: Label {
                    Layout.fillWidth: true
                    Layout.leftMargin: units.gu(6)
                    Layout.rightMargin: units.gu(6)

                    text: modelData
                    font.pointSize: units.dp(10)
                    wrapMode: Label.WordWrap
                }
            }

            ListView {
                id: downloadList
                Layout.fillWidth: true
                Layout.preferredHeight: units.gu(6) * issue.downloads.length

                model: issue.downloads
                delegate: ItemDelegate {
                    id: control
                    width: parent.width
                    height: units.gu(6)

                    ProgressBar {
                        id: progress
                        anchors.centerIn: parent

                        visible: modelData.id == downloading
                        indeterminate: true
                    }

                    // TODO delete issue button
                    RowLayout {
                        visible: modelData.id != downloading

                        anchors {
                            fill: parent
                            leftMargin: units.gu(2)
                            rightMargin: units.gu(2)
                        }
                        spacing: units.gu(1)

                        Ubuntu.Icon {
                            Layout.fillHeight: true
                            Layout.preferredWidth: height
                            Layout.topMargin: units.gu(2)
                            Layout.bottomMargin: units.gu(2)

                            name: modelData.downloaded ? 'stock_ebook' : 'save'
                        }

                        Label {
                            Layout.fillWidth: true
                            Layout.fillHeight: true

                            verticalAlignment: Label.AlignVCenter

                            text: {
                                var text = modelData.lang + ' ' + i18n.tr('PDF')

                                if (modelData.downloaded) {
                                    text = i18n.tr('Read: ') + text;
                                }
                                else {
                                    text = i18n.tr('Download: ') + text;
                                }

                                return text;
                            }
                        }

                        Ubuntu.Icon {
                            Layout.fillHeight: true
                            Layout.preferredWidth: height
                            Layout.topMargin: units.gu(2)
                            Layout.bottomMargin: units.gu(2)

                            name: 'next'
                        }
                    }

                    onClicked: {
                        downloadList.currentIndex = -1;

                        if (!downloading) {
                            if (modelData.downloaded) {
                                stackView.push(Qt.resolvedUrl('ReadPage.qml'), {issue: issue, path: modelData.path})
                            }
                            else {
                                issuePage.downloading = modelData.id;
                                issuePage.downloadUrl = modelData.url;
                                downloader.download(modelData.url);
                            }
                        }
                    }
                }
            }
        }
    }

    SingleDownload {
        id: downloader
        autoStart: true

        onFinished: {
            console.log('download complete', path, issuePage.downloadUrl)
            downloadsDb.putDoc({path: path, url: issuePage.downloadUrl}, issuePage.downloading);

            for (var index in issue.downloads) {
                var download = issue.downloads[index];

                if (download.id == issuePage.downloading) {
                    download.downloaded = true;
                    issue.downloaded = true;
                    download.path = path;
                }
            }

            issuePage.downloading = null;
            issuePage.downloadUrl = null;

            // TODO see about using a proper list model rather than this hack to update the download list
            issuePage.issueChanged();

            stackView.push(Qt.resolvedUrl('ReadPage.qml'), {issue: issue, path: path})
        }
    }
}
